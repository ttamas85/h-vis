<?php
/*
view sql:
SELECT location_id, clinical_study.study_id, start_date, city_clean FROM `location` INNER JOIN clinical_study ON location.study_id=clinical_study.study_id where city_clean IS NOT NULL AND start_date IS NOT NULL


*/



//	header("Content-type:application/json");
//	header("Content-type:text/csv");
	include("../common/db.php");
	
	$mesh_id = $_GET['ids'];
	
	$sql = "SELECT city_clean, country_clean, MIN(start_date) AS start_date, location_cache.study_id FROM location_cache INNER JOIN study_mesh
				ON location_cache.study_id = study_mesh.study_id
				WHERE mesh_id IN ($mesh_id) AND start_date IS NOT NULL AND city_clean IS NOT NULL 
				GROUP BY location_cache.city_clean
				ORDER BY start_date";
	//$sql .= strstr($mesh_id, ',') ? "mesh_id IN ($mesh_id)" : "mesh_id='$mesh_id'";
//echo $sql;
	$query = mysqli_query($server, $sql);
	if ( !$query ) {
		echo mysqli_error($server);
		die;
	}
	
	$cities = array();
	$links = array();
	$id = 0;
	
	while($row = mysqli_fetch_assoc($query)) {
		$city = $row['city_clean'];
		$country = $row['country_clean'];
		$start = $row['start_date'];
		//$cities[] = [$city, $start];
		$cities[$city] = array("id" => $id++, "label" => utf8_encode($city), "country" => utf8_encode($country), "start" => $start );
	}
	
	$cnt = count($cities);
	$values = array_values($cities);
	//echo $cnt;
	for($i=$cnt-1; $i>=1; $i--) {
		$city1 = $values[$i]['label'];
		$start1 = $values[$i]['start'];
		
		$sql = "select min(start_date) as start_date, city_clean from location_cache 
			where study_id in 
			(select distinct study_id from location_cache where city_clean=\"$city1\" 
			and start_date < '$start1') 
			group by city_clean order by start_date, city_clean";

		$res = mysqli_query($server, $sql);
		//echo $sql;
		while($row = mysqli_fetch_assoc($res)) {
			$city2 = $row['city_clean'];
			if(array_key_exists($city2, $cities)) {
				$check = $cities[$city2];
				
				if($check != null && $check['id'] < $i) {
					//values.get(i).setParent(check.getId());
					//values.get(check.getId()).addChild(i);
					
					$links[] = array("source" => $check['id'], "target" => $i);
					break;
				}
			}
		}
	}
	
	$data = array("min" => $values[0]['start'], "max" => $values[$cnt-1]['start'], "count" => $cnt, "mesh" => $mesh_id);
	echo json_encode(array("nodes" => $values, "links" => $links, "data" => $data));
	//echo json_last_error_msg();
	mysqli_close($server);
?>