<?php
header("Content-type:application/json");
include "../common/db.php";

$type = isset($_GET['type']) ? $_GET['type'] : "country";

if($type == "country") {
	$sql = "SELECT * FROM country ORDER BY name";
} else if($type == "region") {
	$sql = "SELECT * FROM region ORDER BY region_name";
} else {
	$sql = "SELECT * FROM mesh_term";
	if($type == "condition") {
		$sql .= " WHERE type=1";
	} else if($type == "intervention") {
		$sql .= " WHERE type=2";
	}
	$sql .= " ORDER BY mesh_term";
}

$query = mysqli_query($server, $sql);
if ( !$query ) {
	echo mysqli_error($server);
	die;
}

$data = [];
while($row = mysqli_fetch_assoc($query)) {
	if($type == "country") {
		$id = $row["country_id"];		
		$name = utf8_encode($row["name"]);
	} else if($type == "region") {
		$id = $row["region_id"];		
		$name = utf8_encode($row["region_name"]);
	} else {
		$id = $row["mesh_id"];		
		$name = utf8_encode($row["mesh_term"]);
	}
	$data[] = ["id" => $id, "name" => $name];
}

echo json_encode($data);
//echo json_last_error_msg();
mysqli_close($server);
?>