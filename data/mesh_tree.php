<?php
include "../common/db.php";

$term = $_GET['term'];
$levels = $_GET['levels'];
$parent = $_GET['parent'];
$type = $_GET['type'];
$root = $term;

if($parent > 0) {
	$levels += $parent;
	for($i=1; $i<=$parent; $i++) {
		$sql = "SELECT * FROM ".$type."_hierarchy WHERE target_node=$root";
		$res = mysqli_query($server, $sql);
		
		if(mysqli_affected_rows($server) > 0) {
			$p = mysqli_fetch_object($res);
			$root = $p->source_node;
		} else {
			break;
		}
	}
}

$data = new stdClass();
$curLevel = 1;
$data->tree = buildTree($root, $curLevel);

echo json_encode($data);

function buildTree($root, $curLevel) {
	global $server, $levels, $term, $type;
	$sql = "SELECT * FROM mesh_term WHERE mesh_id=$root LIMIT 1";
	$res = mysqli_query($server, $sql);
	$row = mysqli_fetch_object($res);

	$data = new stdClass();
	$data->label = $row->mesh_term;
	$data->id = $row->mesh_id;
	if($row->mesh_id == $term) {
		$data->selected = true;
	}
	
	if($curLevel <= $levels) {
		//$curLevel++;
		$sql = "SELECT * FROM ".$type."_hierarchy WHERE source_node=$root";
		$res = mysqli_query($server, $sql);
		
		if(mysqli_affected_rows($server) > 0) {
			$data->children = array();
			while($child = mysqli_fetch_object($res)) {
				$data->children[] = buildTree($child->target_node, $curLevel+1);
			}
		}
	}
	return $data;
}

?>