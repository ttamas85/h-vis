<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);
    
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Év', 'Afrika','Amerika','Ázsia','Európa','Közel-Kelet','Óceánia'],
              ['1990',0,39,3,10,0,0],
              ['1991',1,80,3,16,0,1],
              ['1992',2,105,6,27,0,0],
              ['1993',4,132,7,47,7,3],
              ['1994',3,203,13,41,4,6],
              ['1995',7,269,21,71,6,9],
              ['1996',4,401,24,89,9,23],
              ['1997',11,601,24,133,10,12],
              ['1998',27,831,40,210,24,35],
              ['1999',20,1189,45,251,19,32],
              ['2000',30,1481,82,361,30,52],
              ['2001',55,1821,141,582,49,82],
              ['2002',103,2475,258,856,98,111],
              ['2003',156,3411,523,1295,129,180],
              ['2004',187,4364,778,1789,194,235],
              ['2005',213,5469,1127,2402,279,330],
              ['2006',296,6620,1537,2926,477,363],
              ['2007',299,7231,1954,3527,550,356],
              ['2008',303,8080,2397,4166,721,401],
              ['2009',337,8361,2733,4821,756,406],
              ['2010',365,8492,3085,5112,771,425],
              ['2011',433,8663,3308,5653,868,412],
              ['2012',464,8742,3491,5742,885,451],
              ['2013',491,8999,3463,5836,898,477],
              ['2014',557,9267,3547,6290,887,490],
              ['2015',594,9099,3379,5775,781,506]
            ]);
    
            var options = {
              curveType: 'function',
              legend: {
                  position: 'top',
                  maxLines: 2
              },
              animation: {
                  duration: 500,
                  startup: true
              },
              pointSize: 5,
			  vAxis: {
				  minValue: 0
			  }
            };
    
            var chart = new google.visualization.LineChart(document.getElementById('chart'));
    
            chart.draw(data, options);
          }
        </script>
	</head>
	<body>
		<div id="chart" style="width: 655px; height: 350px"></div>
	</body>
</html>
