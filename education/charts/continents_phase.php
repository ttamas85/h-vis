<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);
    
          function drawChart() {
            var dataEu = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',5065],
				['Fázis2',9159],
				['Fázis3',9637],
				['Fázis4',6725],
				['Egyéb',30633]
            ]);

            var dataAf = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',242],
				['Fázis2',756],
				['Fázis3',1649],
				['Fázis4',634],
				['Egyéb',1993]
            ]);
			
            var dataAm = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',14663],
				['Fázis2',22207],
				['Fázis3',15459],
				['Fázis4',8941],
				['Egyéb',52325]
            ]);
			
            var dataAs = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',2581],
				['Fázis2',5122],
				['Fázis3',9036],
				['Fázis4',4203],
				['Egyéb',12658]
            ]);
			
            var dataMe = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',347],
				['Fázis2',1076],
				['Fázis3',2029],
				['Fázis4',988],
				['Egyéb',4538]
            ]);
			
            var dataPa = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',556],
				['Fázis2',1149],
				['Fázis3',2197],
				['Fázis4',425],
				['Egyéb',1268]
            ]);
    
            var options = {
              title: 'Kutatások fázisonkénti megoszlása - Európa',
              curveType: 'function',
              legend: {
                  position: 'top',
                  maxLines: 2
              },
              animation: {
                  duration: 500,
                  startup: true
              },
              pointSize: 5
            };
    
            var chart = new google.visualization.PieChart(document.getElementById('chart'));
    
            chart.draw(dataEu, options);
			
			$('#africa').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Afrika'
				chart.draw(dataAf, options);
			});

			$('#asia').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Ázsia'
				chart.draw(dataAs, options);
			});

			$('#america').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Amerika'
				chart.draw(dataAm, options);
			});

			$('#middleeast').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Közel-Kelet'
				chart.draw(dataMe, options);
			});

			$('#europe').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Európa'
				chart.draw(dataEu, options);
			});
			
			$('#pacifica').click(function() {
				options.title = 'Kutatások fázisonkénti megoszlása - Óceánia'
				chart.draw(dataPa, options);
			});
          }
        </script>
       	<style>
		  body {
		      background-color: white;
		  }
		  a {
		      color: rgb(158,12,15);
		      font-weight: bold;
		      text-decoration: none;
		  }
		  a:hover {
		      text-decoration: underline;
		  }
		</style>
	</head>
	<body>
		<div>
			Válasszon kontinenst:&emsp;
			<a href='#' id='africa'>Afrika</a>&emsp;
			<a href='#' id='america'>Amerika</a>&emsp;
			<a href='#' id='asia'>Ázsia</a>&emsp;
			<a href='#' id='europe'>Európa</a>&emsp;
			<a href='#' id='middleeast'>Közel-Kelet</a>&emsp;
			<a href='#' id='pacifica'>Óceánia</a>&emsp;
		</div>
		<div id="chart" style="width: 655px; height: 330px"></div>
	</body>
</html>
