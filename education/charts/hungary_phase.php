<html>
	<head>
		<meta charset="utf-8">
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);
    
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
				['Fázis', 'Kutatások száma'],
				['Fázis1',59],
				['Fázis2',654],
				['Fázis3',1573],
				['Fázis4',207],
				['Egyéb',370]
            ]);
  
            var options = {
              curveType: 'function',
              legend: {
                  position: 'top',
                  maxLines: 2
              },
              animation: {
                  duration: 500,
                  startup: true
              },
              pointSize: 5
            };
    
            var chart = new google.visualization.PieChart(document.getElementById('chart'));
    
            chart.draw(data, options);
          }
        </script>
	</head>
	<body>
		<div id="chart" style="width: 655px; height: 350px"></div>
	</body>
</html>
