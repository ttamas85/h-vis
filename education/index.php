<html>
	<head>
		<link rel="stylesheet" href="../css/style.css" />
	</head>
	<body>
		<h1>Tájékozatató anyagok a klinikai vizsgálatokról</h1>
		<div id="main_menu">
			<h3><a href="elearning/story.html">Tájékoztató anyag indítása</a></h3>
			<h2>Térképek</h2>
			<h3><a href="maps/map_city.php">Magyarországi települések kutatási kapcsolatai</a></h3>
			<h3><a href="maps/map_mesh.php">Magyarországi kutatások hatóanyag/betegség szerint</a></h3>
			<h2>Diagramok</h2>
			<h3><a href="charts/literature.php">Orvosi publikációk évenkénti száma</a></h3>
			<h3><a href="charts/continents.php">Vizsgálatok száma évenként és kontinensenként</a></h3>
			<h3><a href="charts/continents_phase.php">Vizsgálatok száma fázisonként és kontinensenként</a></h3>
			<h3><a href="charts/hungary_phase.php">Magyarországi vizsgálatok száma fázisonként</a></h3>
			<br>
			<h3><a href="../index.html">Vissza</a></h3>
		</div>
	</body>
</html>