<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="http://www.amcharts.com/lib/4/core.js"></script>
		<script src="http://www.amcharts.com/lib/4/maps.js"></script>
		<script src="http://www.amcharts.com/lib/4/geodata/hungaryHigh.js"></script>
		<script src="../../dhtmlxSuite_v50/codebase/dhtmlx.js"></script>
		<link rel="stylesheet" type="text/css" href="../../dhtmlxSuite_v50/codebase/dhtmlx.css">

		<script>
			var map;
			var imageSeries, lineSeries;
			var nodes;
			var label;
			var meshTree;

			loadCities = function() {
				$.ajax({
					url: 'map_data_cities.php',
					dataType: 'json',
				}).done(function(result) {
					imageSeries.mapImages.clear();
    				
					var c = result.cities;
					nodes = {};

					for(var i=0; i<c.length; i++) {
						var city = c[i];

						var mi = imageSeries.mapImages.create();
						mi.latitude = city.latitude;
						mi.longitude = city.longitude;
						mi.title = city.title;
						mi.cid = city.id;
						mi.tooltipText = city.title;

						nodes[city.id] = mi;
					}
				});
			}
		
			$(document).ready(function() {

				/*meshTree = new dhtmlXTreeView({
					parent: "mesh_tree",
					checkboxes: true,
					skin: "material",
					xml: "mesh_tree.xml"
 				});*/
 				meshTree = new dhtmlXTreeObject("mesh_tree","100%","100%",0);
				meshTree.setImagePath("../../dhtmlxSuite_v50/codebase/imgs/dhxtree_material/");
				meshTree.enableCheckBoxes(1);
				meshTree.enableThreeStateCheckboxes(true);
				//meshTree.setXMLAutoLoading("mesh_tree.php");
				meshTree.enableSmartXMLParsing(true);
				meshTree.load("mesh_tree.xml", "xml");


				map = am4core.create("chartdiv", am4maps.MapChart);
				map.geodata = am4geodata_hungaryHigh;
				map.projection = new am4maps.projections.Miller();
				var polygonSeries = map.series.push(new am4maps.MapPolygonSeries());
				polygonSeries.useGeodata = true;
				label = map.chartContainer.createChild(am4core.Label);
				//label.text = "franceLow";
				
				map.events.on("clickMapObject", function(event) {
                  alert('Clicked ID: ' + event.mapObject.id + ' (' + event.mapObject.title + ')');
                });

				// Create image series
				imageSeries = map.series.push(new am4maps.MapImageSeries());

				imageSeries.mapImages.template.events.on("hit", function(ev) {
					refreshMap(ev.target.title, ev.target.cid);
	                 // alert('Clicked ID: ' + event.mapObject.id + ' (' + event.mapObject.title + ')');
				});
				
				// Create a circle image in image series template so it gets replicated to all new images
				var imageSeriesTemplate = imageSeries.mapImages.template;
				var circle = imageSeriesTemplate.createChild(am4core.Circle);
				circle.radius = 4;
				circle.fill = am4core.color("#B27799");
				circle.stroke = am4core.color("#FFFFFF");
				circle.strokeWidth = 2;
				circle.nonScaling = true;
				circle.tooltipText = "{title}";

				// Set property fields
				imageSeriesTemplate.propertyFields.latitude = "latitude";
				imageSeriesTemplate.propertyFields.longitude = "longitude";

				// Add line series
				lineSeries = map.series.push(new am4maps.MapLineSeries());

				map.smallMap = new am4maps.SmallMap();
				map.smallMap.series.push(polygonSeries);

				map.zoomControl = new am4maps.ZoomControl();
				map.zoomControl.slider.height = 100;

				loadCities();
				
				$('#refresh_map').click(function() {
					label_text = $('#city_select option:selected').text();
					cid =  $('#city_select').val();

					refreshMap(label_text, cid);
				});
				
				$('#refresh_map_tree').click(function() {
					mid = meshTree.getAllChecked();

					refreshMapCat("", mid);
				});
				
				$('#refresh_map_mesh').click(function() {
					label_text = $('#mesh_cat_select option:selected').text();
					mid =  $('#mesh_cat_select').val();

					refreshMapCat(label_text, mid);
				});

				refreshMap = function(label_text, cid) {
					label.text = label_text;
    				$.ajax({
    					url: 'map_data_connection_for_city.php',
    					dataType: 'json',
    					data: {
    						cid: cid
    					}
    				}).done(function(result) {
						lineSeries.mapLines.clear();
        				
    					var c = result.connections;
    
    					for(var i=0; i<c.length; i++) {
    						var conn = c[i];
    
    						var c1 = conn.c1;
    						var c2 = conn.c2;
    
    						var line = lineSeries.mapLines.create();
    						line.imagesToConnect = [nodes[c1], nodes[c2]];
    						line.strokeWidth = Math.min(conn.cnt, 10)/2;
    					}
    				});
				}
				
				refreshMapCat = function(label_text, mid) {
					label.text = label_text;
    				$.ajax({
    					url: 'map_data_connection_for_mesh.php',
    					dataType: 'json',
    					data: {
    						mesh_cat: mid
    					},
    					method: "POST"
    				}).done(function(result) {
						lineSeries.mapLines.clear();
        				
    					var c = result.connections;
    
    					for(var i=0; i<c.length; i++) {
    						var conn = c[i];
    
    						var c1 = conn.c1;
    						var c2 = conn.c2;
    
    						var line = lineSeries.mapLines.create();
    						line.imagesToConnect = [nodes[c1], nodes[c2]];
    						line.strokeWidth = Math.min(conn.cnt, 10)/2;
    					}
    				});
				}
			});
		</script>
	</head>
	<body>
		<div style="float:right;height:95vh">
			<div id="mesh_tree" style="width:300px;height:90%;"></div>
			<div>
				<button id="refresh_map_tree">Térkép betöltése</button>
			</div>
		</div>
		<div>
    		<div>
    			<p>Település kiválasztása:
    			<select id='city_select'>
    				<?php 
    				    require '../../common/db.php';
    				    
    				    $cities = mysqli_query($server, "SELECT * FROM city WHERE country_id=141 AND lat IS NOT NULL ORDER BY city_name");
    				    while($row = mysqli_fetch_assoc($cities)) {
    				        $name = $row["city_name"];
    				        $id = $row["city_id"];
    				        
    				        echo "<option value='$id'>$name</option>";
    				    }
    				?>
    			</select>
    			<button id='refresh_map'>Térkép frissítése</button>
    			<em>Vagy kattintson egy településre a térképen!</em>
    		</div>
    		<div>
    			<p>Kategória kiválasztása:
    			<select id='mesh_cat_select'>
    				<?php 
    				    $cities = mysqli_query($server, "SELECT * FROM mesh_tree WHERE tree LIKE 'C__' OR tree LIKE 'D__' ORDER BY tree");
    				    while($row = mysqli_fetch_assoc($cities)) {
    				        $code = $row["tree"];
    				        $desc = $row["description"];
    				        
    				        echo "<option value='$code'>$code - $desc</option>";
    				    }
    				?>
    			</select>
    			<button id='refresh_map_mesh'>Térkép frissítése</button>
    		</div>
    		<div id="chartdiv" style="height:500px;width:800px"></div>
		</div>
	</body>
</html>