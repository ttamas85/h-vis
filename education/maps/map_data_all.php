<?php 
    require '../../common/db.php';
    
    $city = $_REQUEST['cid'];
    
	$cities = mysqli_query($server, "SELECT * FROM city WHERE country_id=141 AND lat IS NOT NULL");
	
	$data = array();
	$data["cities"] = array();
	$data["connections"] = array();
	
	while($row = mysqli_fetch_assoc($cities)) {
	    $lat = $row["lat"];
	    $lon = $row["lon"];
	    $name = $row["city_name"];
	    $id = $row["city_id"];
	    
	    $data["cities"][] = array('id' => $id,
 	                              'latitude' => (float)$lat,
	        	                  'longitude' => (float)$lon,
	                              'title' => $name);
	    
	}

	$connections = mysqli_query($server, "SELECT count(*) AS cnt, city_id1, city_id2 FROM `city_connection` WHERE country_id1=141 AND country_id2=141 AND (city_id1=$city OR city_id2=$city) GROUP BY city_id1, city_id2");
	
	while($row = mysqli_fetch_assoc($connections)) {
	    $c1 = $row['city_id1'];
	    $c2 = $row['city_id2'];
	    $cnt = $row['cnt'];
	    
	    $data["connections"][] = array(
	        'c1' => $c1,
	        'c2' => $c2,
	        'cnt' => $cnt);
	    
	}
	
	header("Content-Type: text/json");
	echo json_encode($data);
?>

