<?php 
    require '../../common/db.php';
        
	$cities = mysqli_query($server, "SELECT * FROM city WHERE country_id=141 AND lat IS NOT NULL");
	
	$data = array();
	$data["cities"] = array();
	
	while($row = mysqli_fetch_assoc($cities)) {
	    $lat = $row["lat"];
	    $lon = $row["lon"];
	    $name = $row["city_name"];
	    $id = $row["city_id"];
	    
	    $data["cities"][] = array('id' => $id,
 	                              'latitude' => (float)$lat,
	        	                  'longitude' => (float)$lon,
	                              'title' => $name);
	    
	}
	
	header("Content-Type: text/json");
	echo json_encode($data);
?>

