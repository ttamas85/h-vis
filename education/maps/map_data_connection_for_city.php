<?php 
    require '../../common/db.php';
    
    $city = $_REQUEST['cid'];
    	
	$data = array();
	$data["connections"] = array();

	$connections = mysqli_query($server, "SELECT count(*) AS cnt, city_id1, city_id2 FROM `city_connection` WHERE country_id1=141 AND country_id2=141 AND (city_id1=$city OR city_id2=$city) GROUP BY city_id1, city_id2");
	
	while($row = mysqli_fetch_assoc($connections)) {
	    $c1 = $row['city_id1'];
	    $c2 = $row['city_id2'];
	    $cnt = $row['cnt'];
	    
	    $data["connections"][] = array(
	        'c1' => $c1,
	        'c2' => $c2,
	        'cnt' => $cnt);
	    
	}
	
	header("Content-Type: text/json");
	echo json_encode($data);
?>

