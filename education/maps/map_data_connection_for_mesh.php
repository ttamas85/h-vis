<?php 
    require '../../common/db.php';
    
    $mesh = $_REQUEST['mesh_cat'];
    	
	$data = array();
	$data["connections"] = array();

	if(substr($mesh, 0, 1 ) === "C" || substr($mesh, 0, 1 ) === "D") {
	   $connections = mysqli_query($server, "SELECT count(*) AS cnt, city_id1, city_id2 FROM `city_connection` WHERE country_id1=141 AND country_id2=141 AND study_id IN (SELECT study_id FROM study_mesh AS sm INNER JOIN mesh_term AS mt ON sm.mesh_id=mt.mesh_id WHERE mt.category='$mesh') GROUP BY city_id1, city_id2");
	} else {
	    $connections = mysqli_query($server, "SELECT count(*) AS cnt, city_id1, city_id2 FROM `city_connection` WHERE country_id1=141 AND country_id2=141 AND study_id IN (SELECT study_id FROM study_mesh AS sm WHERE mesh_id IN (SELECT mesh_id FROM mesh_term WHERE tree_id IN ($mesh))) GROUP BY city_id1, city_id2");
	}
	
	while($row = mysqli_fetch_assoc($connections)) {
	    $c1 = $row['city_id1'];
	    $c2 = $row['city_id2'];
	    $cnt = $row['cnt'];
	    
	    $data["connections"][] = array(
	        'c1' => $c1,
	        'c2' => $c2,
	        'cnt' => $cnt);
	    
	}
	
	header("Content-Type: text/json");
	echo json_encode($data);
?>

