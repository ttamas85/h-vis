<?php 
header("Content-Type: text/xml")
?>
<tree id="0">
<?php 
    require "../../common/db.php";
    
    function getChildren($parent) {
        global $server;
        $data = mysqli_query($server, "SELECT * FROM mesh_tree WHERE parent_id=$parent");

        while($row = mysqli_fetch_assoc($data)) {
            $child = $row['id'];
            $code = $row['tree'];
            $desc = $row['description'];
            
            echo "<item id='$child' text=\"$code - $desc\">";
            getChildren($child);
            echo "</item>";
        }
    }
    
    
    $nodes = mysqli_query($server, "SELECT * FROM mesh_tree WHERE tree LIKE 'C__' OR tree LIKE 'D__'");
    while($row = mysqli_fetch_assoc($nodes)) {
        $child = $row['id'];
        $code = $row['tree'];
        $desc = $row['description'];
        
        echo "<item id=\"$child\" text=\"$code - $desc\">";       
        getChildren($child);
        echo "</item>";
    }
?>
</tree>