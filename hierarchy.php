<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/style.css" />

		<script src="https://d3js.org/d3.v4.min.js"></script>
		<script src="js/drawing.js"></script>
		<script src="js/controls.js"></script>
	</head>
	<body>
		<div id="controls">
			<p>
				<span class="control_box">
					Term:
					<select id="term">
					<?php
						include "common/db.php";
						
						$sql = "SELECT * FROM mesh_term WHERE type=";
						$sql .= $_GET['type'] == 'condition' ? 1 : 2;
						$sql .= " ORDER BY mesh_term";
						$terms = mysqli_query($server, $sql);
						
						while($term = mysqli_fetch_object($terms)) {
							echo "<option value='$term->mesh_id'>$term->mesh_term</option>";
						}
					
					?>
					</select>
				</span>
			</p>
			<p>
				<span class="control_box">Child levels: <input id="tree_levels" type="number" min="0" value="2" style="width: 4em"></span>
				<span class="control_box">Parent levels: <input id="parent_levels" type="number" min="0" value="0" style="width: 4em"></span>
				<span class="control_box"><button onclick="loadHierarchy('<?php echo $_GET['type']; ?>')">Show hierarchy</button></span>
				<span class="control_box">
					Diagram size:
					<input id="svg_width" value="960" type="number"  style="width: 4em" /> X <input id="svg_height" value="600" type="number"  style="width: 4em"/>
					<button id="svgButton" onclick="setSvgSize()">Set size</button>
				</span>
			</p>
			<p>
				<span class="control_box"><button onclick="drawFromSelection('graph')">Graph from selection</button></span>
				<span class="control_box"><button onclick="drawFromSelection('tree')">Tree from selection</button></span>
				<span class="control_box" id="main_box"><button id="mainButton" onclick="location.href='index.html'">Main menu</button></span>
			</p>
		</div>
		<svg width="960" height="600"></svg>
	</body>
</html>