var gridOpen = true;
function toggleGrid() {
	var grid = document.getElementById("gridbox");
	grid.style.display = gridOpen ? "none" : "block";
	document.getElementById("gridButton").innerHTML = gridOpen ? "Show table" : "Hide table";
	gridOpen = !gridOpen;
}

function setSvgSize() {
	var width = document.getElementById("svg_width").value;
	var height = document.getElementById("svg_height").value;
	
	svg = d3.select("svg")
		.attr("width", width)
		.attr("height", height);
}

function loadHierarchy(type) {
	var term = document.getElementById("term").value;
	var levels = document.getElementById("tree_levels").value;
	var parent_levels = document.getElementById("parent_levels").value;
	selectedIds = [];
	
	showHierarchy(term, levels, parent_levels, type);
}

function drawFromSelection(type) {
	if(selectedIds.length < 1) {
		alert("Select at least one node!");
	} else {
		var url = "diffusion_popup.html?type="+type+"&ids="+selectedIds.join(',');
		window.open(url);
	}
}

function loadImageFromParams() {
	var params = {};

	if (location.search) {
		var parts = location.search.substring(1).split('&');

		for (var i = 0; i < parts.length; i++) {
			var nv = parts[i].split('=');
			if (!nv[0]) continue;
			params[nv[0]] = nv[1] || true;
		}
	}

	var type = params['type'];
	var ids = params['ids'];
	
	showImage(ids, type, 0, true);
}