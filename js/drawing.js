var svg, simulation;
var selectedIds = [];

function showTree(mesh) {
	var tree = document.getElementById("tree_select_"+mesh).value;
	showImage(mesh, "tree", tree);
}

function showImage(mesh, type, tree=0, popup=false) {
	//var type = document.getElementById("type_graph").checked ? "graph" : "tree";
	//var mesh = document.getElementById("mesh").value;

	var url;
	if(popup) {
		if(type == "graph") {
			url = "data/diffusion_graph.php?ids="+mesh;
		} else {
			url = "data/diffusion_tree.php?ids="+mesh;
		}
	} else {
		url = "data/diffusion/";
		if(type == "graph") {
			url += "graph/"+mesh+"_graph.json";
		} else {
			//var tree = document.getElementById("tree").value;
			url += "tree/"+mesh+"_tree_"+tree+".json";
		}
	}
	
	svg = d3.select("svg"),
		width = +svg.attr("width"),
		height = +svg.attr("height");
	svg.selectAll("*").remove();
	
	simulation = d3.forceSimulation()
		.force("link", d3.forceLink().id(function(d) { return d.id; }))
		.force("charge", d3.forceManyBody())
		.force("center", d3.forceCenter(width / 2, height / 2));		

	
	d3.json(url, function(error, json) {
		var warn = document.getElementById("warning");
		if(warn != null) {
			warn.style.display = "none";
		}
		if (error) {
			document.getElementById("error").style.display = "block";
			document.getElementById("error_text").innerHTML = error;
			throw error;
		}
		
		var color = d3.scaleTime().domain([new Date(json.data.min),new Date(json.data.max)]).range(["#FFFF00", "#FF0000"]);
		document.getElementById("legend-start").innerHTML = json.data.min;
		document.getElementById("legend-end").innerHTML = json.data.max;
		
		if(type == "graph") {

			defs = svg.append("defs")

			defs.append("marker")
				.attr("id", "arrow")
				.attr("viewBox", "0 -5 10 10")
				.attr("refX", 24)
				.attr("refY", 0)
				.attr("markerWidth", 4)
				.attr("markerHeight",4)
				.attr("orient", "auto")
				.append("path")
					.attr("d", "M0,-5L10,0L0,5")
					.attr("class","arrowHead");
		
			var link = svg.append("g")
				.attr("class", "links")
				.selectAll("line")
				.data(json.links)
				.enter().append("line")
					.attr("marker-end", "url(#arrow)");
				  //.attr("stroke-width", function(d) { return Math.sqrt(d.value); });

		
			var graphnode = svg.append("g")
					.attr("class", "nodes")
					.selectAll("circle")
					.data(json.nodes)
					.enter()
					.append("g")
					.attr("class", "graphnode");
					
			var node = graphnode.append("circle")
					.attr("r", 5)
					.attr("fill", function(d) { return color(new Date(d.start)); })
					.call(d3.drag()
						.on("start", dragstarted)
						.on("drag", dragged)
						.on("end", dragended));
					
			console.log(node);
			graphnode.append('text')
				.attr("dy", "-3")
				.text(function(d) {
					return d.label;
				});
		
			graphnode.append("title")
				.text(function(d) { return d.label+", "+d.country+" ("+d.start+")"; });
		
			simulation
				.nodes(json.nodes)
				.on("tick", ticked);

			simulation.force("link")
				.links(json.links);
		  
			function ticked() {
				link
					.attr("x1", function(d) { return d.source.x; })
					.attr("y1", function(d) { return d.source.y; })
					.attr("x2", function(d) { return d.target.x; })
					.attr("y2", function(d) { return d.target.y; });

				graphnode
					.attr("transform", function (d) {return "translate(" + d.x + ", " + d.y + ")";});
					//.attr("cx", function(d) { return d.x; })
					//.attr("cy", function(d) { return d.y; });
			}
		} else {
			var treee, hierarchy, nodeMap, links;
			var i = 0,
			duration = 750,
			root;

			var margin = {top: 20, right: 90, bottom: 30, left: 190};
			
			svg = d3.select("svg")
				//.attr("width", width + margin.right + margin.left)
				//.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate("
					+ margin.left + "," + margin.top + ")");
			
			// declares a tree layout and assigns the size
			var treemap = d3.tree().size([height, width]);

			// Assigns parent, children, height, depth
			var jsonTree = Array.isArray(json.tree) ? json.tree[0] : json.tree;
			root = d3.hierarchy(jsonTree, function(d) { return d.children; });
			root.x0 = height / 2;
			root.y0 = 0;	
				
			update(root);

			function update(source) {
				// Assigns the x and y position for the nodes
				  var treeData = treemap(root);

				  // Compute the new tree layout.
				  var nodes = treeData.descendants(),
					  links = treeData.descendants().slice(1);

				  // Normalize for fixed-depth.
				  nodes.forEach(function(d){ d.y = d.depth * 180});

				  // ****************** Nodes section ***************************

				  // Update the nodes...
				  var node = svg.selectAll('g.node')
					  .data(nodes, function(d) {return d.id || (d.id = ++i); });

				  // Enter any new modes at the parent's previous position.
				  var nodeEnter = node.enter().append('g')
					  .attr('class', 'node')
					  .attr("transform", function(d) {
						return "translate(" + source.y0 + "," + source.x0 + ")";
					})
					.on('click', click);

				  // Add Circle for the nodes
				  nodeEnter.append('circle')
					  .attr('class', 'node')
					  .attr('r', 1e-6)
					  .style("stroke", function(d) {
						  return color(new Date(d.data.start));;
					  })
					  .style("fill", function(d) {
						  return d._children ? "#FFE4B5" : "#fff";
					  });

				  // Add labels for the nodes
				  nodeEnter.append('text')
					  .attr("dy", ".35em")
					  .attr("x", function(d) {
						  return d.children || d._children ? -13 : 13;
					  })
					  .attr("text-anchor", function(d) {
						  return d.children || d._children ? "end" : "start";
					  })
					  .text(function(d) { return d.data.label + " (" + d.data.country + ")"; });

				  // UPDATE
				  var nodeUpdate = nodeEnter.merge(node);

				  // Transition to the proper position for the node
				  nodeUpdate.transition()
					.duration(duration)
					.attr("transform", function(d) { 
						return "translate(" + d.y + "," + d.x + ")";
					 });

				  // Update the node attributes and style
				  nodeUpdate.select('circle.node')
					.attr('r', 10)
					.style("fill", function(d) {
						return d._children ? "#FFE4B5" : "#fff";
					})
					.attr('cursor', 'pointer');


				  // Remove any exiting nodes
				  var nodeExit = node.exit().transition()
					  .duration(duration)
					  .attr("transform", function(d) {
						  return "translate(" + source.y + "," + source.x + ")";
					  })
					  .remove();

				  // On exit reduce the node circles size to 0
				  nodeExit.select('circle')
					.attr('r', 1e-6);

				  // On exit reduce the opacity of text labels
				  nodeExit.select('text')
					.style('fill-opacity', 1e-6);

				  // ****************** links section ***************************

				  // Update the links...
				  var link = svg.selectAll('path.link')
					  .data(links, function(d) { return d.id; });

				  // Enter any new links at the parent's previous position.
				  var linkEnter = link.enter().insert('path', "g")
					  .attr("class", "link")
					  .attr('d', function(d){
						var o = {x: source.x0, y: source.y0}
						return diagonal(o, o)
					  });

				  // UPDATE
				  var linkUpdate = linkEnter.merge(link);

				  // Transition back to the parent element position
				  linkUpdate.transition()
					  .duration(duration)
					  .attr('d', function(d){ return diagonal(d, d.parent) });

				  // Remove any exiting links
				  var linkExit = link.exit().transition()
					  .duration(duration)
					  .attr('d', function(d) {
						var o = {x: source.x, y: source.y}
						return diagonal(o, o)
					  })
					  .remove();

				  // Store the old positions for transition.
				  nodes.forEach(function(d){
					d.x0 = d.x;
					d.y0 = d.y;
				  });

				  // Creates a curved (diagonal) path from parent to the child nodes
				  function diagonal(s, d) {

					path = `M ${s.y} ${s.x}
							C ${(s.y + d.y) / 2} ${s.x},
							  ${(s.y + d.y) / 2} ${d.x},
							  ${d.y} ${d.x}`

					return path
				  }

				  // Toggle children on click.
				  function click(d) {
					if (d.children) {
						d._children = d.children;
						d.children = null;
					  } else {
						d.children = d._children;
						d._children = null;
					  }
					update(d);
				  }
			}

		}
	});
}

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(10).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

function showHierarchy(term, levels, parent_levels, type) {
	var url = "data/mesh_tree.php?type="+type+"&term="+term+"&levels="+levels+"&parent="+parent_levels;
	
	svg = d3.select("svg"),
		width = +svg.attr("width"),
		height = +svg.attr("height");
	svg.selectAll("*").remove();
	
	d3.json(url, function(error, json) {
		if (error) throw error;
		
		//var color = d3.scaleTime().domain([new Date(json.data.min),new Date(json.data.max)]).range(["#FFFF00", "#FF0000"]);
			var treee, hierarchy, nodeMap, links;
			var i = 0,
			duration = 750,
			root;

			var margin = {top: 20, right: 90, bottom: 30, left: 190};
			
			svg = d3.select("svg")
				//.attr("width", width + margin.right + margin.left)
				//.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate("
					+ margin.left + "," + margin.top + ")");
			
			// declares a tree layout and assigns the size
			var treemap = d3.tree().size([height, width]);

			// Assigns parent, children, height, depth
			root = d3.hierarchy(json.tree, function(d) { return d.children; });
			root.x0 = height / 2;
			root.y0 = 0;	
				
			update(root);

			function update(source) {
				// Assigns the x and y position for the nodes
				  var treeData = treemap(root);

				  // Compute the new tree layout.
				  var nodes = treeData.descendants(),
					  links = treeData.descendants().slice(1);

				  // Normalize for fixed-depth.
				  nodes.forEach(function(d){ d.y = d.depth * 180});

				  // ****************** Nodes section ***************************

				  // Update the nodes...
				  var node = svg.selectAll('g.node')
					  .data(nodes, function(d) {return d.id || (d.id = ++i); });

				  // Enter any new modes at the parent's previous position.
				  var nodeEnter = node.enter().append('g')
					  .attr('class', 'node')
					  .attr("transform", function(d) {
						return "translate(" + source.y0 + "," + source.x0 + ")";
					})
					.on('click', click);

				  // Add Circle for the nodes
				  nodeEnter.append('circle')
					  .attr('class', 'node')
					  .attr('r', 1e-6)
					  .style("stroke", function(d) {
						  return d.data.selected ? "red" : "steelblue";})
					  .style("fill", "#fff");

				  // Add labels for the nodes
				  nodeEnter.append('text')
					  .attr("dy", ".35em")
					  .attr("x", function(d) {
						  return d.children || d._children ? -13 : 13;
					  })
					  .attr("text-anchor", function(d) {
						  return d.children || d._children ? "end" : "start";
					  })
					  .text(function(d) { return d.data.label; });

				  // UPDATE
				  var nodeUpdate = nodeEnter.merge(node);

				  // Transition to the proper position for the node
				  nodeUpdate.transition()
					.duration(duration)
					.attr("transform", function(d) { 
						return "translate(" + d.y + "," + d.x + ")";
					 });

				  // Update the node attributes and style
				  nodeUpdate.select('circle.node')
					.attr('r', 10)
					.style("fill", function(d) {
						return d._checked ? (d.data.selected ? "red" : "steelblue") : "#fff";
					})
					.attr('cursor', 'pointer');


				  // Remove any exiting nodes
				  var nodeExit = node.exit().transition()
					  .duration(duration)
					  .attr("transform", function(d) {
						  return "translate(" + source.y + "," + source.x + ")";
					  })
					  .remove();

				  // On exit reduce the node circles size to 0
				  nodeExit.select('circle')
					.attr('r', 1e-6);

				  // On exit reduce the opacity of text labels
				  nodeExit.select('text')
					.style('fill-opacity', 1e-6);

				  // ****************** links section ***************************

				  // Update the links...
				  var link = svg.selectAll('path.link')
					  .data(links, function(d) { return d.id; });

				  // Enter any new links at the parent's previous position.
				  var linkEnter = link.enter().insert('path', "g")
					  .attr("class", "link")
					  .attr('d', function(d){
						var o = {x: source.x0, y: source.y0}
						return diagonal(o, o)
					  });

				  // UPDATE
				  var linkUpdate = linkEnter.merge(link);

				  // Transition back to the parent element position
				  linkUpdate.transition()
					  .duration(duration)
					  .attr('d', function(d){ return diagonal(d, d.parent) });

				  // Remove any exiting links
				  var linkExit = link.exit().transition()
					  .duration(duration)
					  .attr('d', function(d) {
						var o = {x: source.x, y: source.y}
						return diagonal(o, o)
					  })
					  .remove();

				  // Store the old positions for transition.
				  nodes.forEach(function(d){
					d.x0 = d.x;
					d.y0 = d.y;
				  });

				  // Creates a curved (diagonal) path from parent to the child nodes
				  function diagonal(s, d) {

					path = `M ${s.y} ${s.x}
							C ${(s.y + d.y) / 2} ${s.x},
							  ${(s.y + d.y) / 2} ${d.x},
							  ${d.y} ${d.x}`

					return path
				  }

				  // Toggle children on click.
				  function click(d) {
					if(!d._checked) {
						selectedIds.push(d.data.id);
						d._checked = true;
					} else {
						var index = selectedIds.indexOf(d.data.id);
						if(index > -1) {
							selectedIds.splice(index, 1);
						}
						d._checked = false;
					}
					update(d);
				  }
			}
	});
}