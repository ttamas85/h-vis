function eXcell_graph(cell){  // the eXcell name is defined here
	if (cell){                  // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.edit = function(){}   // read-only cell doesn't have edit method
	// the cell is read-only, so it's always in the disabled state
	this.isDisabled = function(){ return true; }
	this.setValue=function(val){
		var row_id=this.cell.parentNode.childNodes[0].innerHTML;
		var tree_count=this.cell.parentNode.childNodes[8].innerHTML;
		row_id = ("00000"+row_id).slice(-6);
		var html = "<button onclick='showImage(\""+row_id+"\", \"graph\")'>Graph</button>";
		if(tree_count > 0) {
			if(tree_count > 1) {
				html += "<select id='tree_select_"+row_id+"'>";
				for(var i=1; i<=tree_count; i++) {
					html += "<option value='"+i+"'>"+i+"</option>";
				}
				html += "</select>";
				html += "<button onclick='showTree(\""+row_id+"\")'>Tree</button>";
			} else {
				html += "<button onclick='showImage(\""+row_id+"\", \"tree\", 1)'>Tree</button>";
			}
		}
		html += "<button onclick='window.open(\"study_list.php?mesh_id="+row_id+"\")'>Studies</button>";
		html += "<button onclick='window.open(\"map_mesh.php?mesh_id="+row_id+"\")'>Map</button>";
		this.setCValue(html,val);                                      
	}
}
eXcell_graph.prototype = new eXcell; // nests all other methods from the base class

function eXcell_type(cell){  // the eXcell name is defined here
	if (cell){                  // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.edit = function(){}   // read-only cell doesn't have edit method
	// the cell is read-only, so it's always in the disabled state
	this.isDisabled = function(){ return true; }
	this.setValue=function(val){
		if(val==1) {
			this.setCValue("Cond",val);
		} else if(val==2) {
			this.setCValue("Int",val);
		}
	}
}
eXcell_type.prototype = new eXcell; // nests all other methods from the base class

grid = new dhtmlXGridObject('gridbox');
grid.csv.cell = ";";
grid.enableSmartRendering(true);
grid.setHeader("Id,Mesh term,Type,Start date,End date,Cities,Links,Orphans,Trees,Controls");//the headers of columns  
grid.setInitWidths("80,*,80,90,90,60,60,60,60,240");          //the widths of columns  
grid.setColAlign("left,left,center,center,center,center,center,center,center,center");       //the alignment of columns   
grid.setColTypes("ro,ro,type,ro,ro,ron,ron,ron,ron,graph");                //the types of columns  
grid.setColSorting("int,str,int,date,date,int,int,int,int");          //the sorting types   	
grid.attachHeader(",#text_filter,#text_filter,,,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter");

grid.init();
grid.load("data/diffusion/summary.csv","csv");