<?php
	include "common/db.php";
	
	$type = $_GET['type'];
	$values = $_GET['values'];

	if($type == 'country') {	
		$csql = "SELECT * FROM country WHERE country_id = $values";		
		$query = mysqli_query($server, $csql);
		if ( !$query ) {
			echo mysqli_error($server);
			die;
		}	
		
		$row = mysqli_fetch_assoc($query);
		$map_code = $row['country_code'];
		$name = $row['name'];
		
		$sql = "SELECT city_clean AS cat, count(*) as cnt FROM location_cache WHERE country_id = $values
				GROUP BY city_clean
				ORDER BY cnt DESC";
		
	} else if($type == 'region') {
		$rsql = "SELECT * FROM region WHERE region_id = $values";		
		$query = mysqli_query($server, $rsql);
		if ( !$query ) {
			echo mysqli_error($server);
			die;
		}	
		
		$row = mysqli_fetch_assoc($query);
		$map_code = $row['region_code'];
		$name = $row['region_name'];
		
		$sql = "SELECT c.name AS cat, count(*) as cnt FROM location_country AS lc INNER JOIN country AS c ON c.country_id=lc.country_id 
				WHERE lc.country_id IN (SELECT country_id FROM country WHERE region_id = $values)
				GROUP BY c.country_id
				ORDER BY cnt DESC";
	}
//echo $sql;	
	$query = mysqli_query($server, $sql);

	if ( !$query ) {
		echo mysqli_error($server);
		die;
	}
?>
<html>
	<head>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.charts.load('current', {'packages':['geochart'],
			mapsApiKey:'AIzaSyBd-Lc2C2cLEwNkXYMtaT7l7bGrHVVa02k'});
			google.charts.setOnLoadCallback(drawRegionsMap);

			function drawRegionsMap() {
			   var data = google.visualization.arrayToDataTable([
				["Location","<?php echo $type=='country' ? 'Location count' : 'Study count'; ?>"  ],
				<?php
					while($row = mysqli_fetch_assoc($query)) {
						$key = $row["cat"];
						$cnt = $row["cnt"];

						echo "[\"$key\", $cnt],";
					}
				?>

				]);

				var options = {
					sizeAxis: { minValue: 0, maxValue: 2000 },
					region: '<?php echo $map_code; ?>',
					displayMode: '<?php echo $type=='country' ? 'markers' : 'regions'; ?>',
					colorAxis: {colors: ['lightblue', 'darkblue']},
					
					resolution:  '<?php echo $type=='country' ? 'provinces' : 'countries'; ?>'
				};

				var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

				chart.draw(data, options);
			}
		</script>
		<style>
			path {
				stroke: black;
				stroke-width: 1px;
			}
		</style>
	</head>
	<body>
		<h1>Selected <?php echo $type;?>: <?php echo $name; ?></h1>
		<div id="regions_div" style="width: 1300px; height: 800px;"></div>
	</body>
</html>