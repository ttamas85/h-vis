<?php
	include "common/db.php";
	
	$term = $_GET['mesh_id'];

	mysqli_query($server, "SET NAMES UTF8");
	
	$sql = "SELECT city_clean AS cat, count(*) as cnt FROM location_cache WHERE study_id IN (
				SELECT study_id FROM study_mesh WHERE mesh_id=$term)
			GROUP BY city_clean
			ORDER BY cnt DESC";
		
	$query = mysqli_query($server, $sql);

	if ( !$query ) {
		echo mysqli_error($server);
		die;
	}
?>
<html>
	<head>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.charts.load('current', {'packages':['geochart'],
			mapsApiKey:'AIzaSyBd-Lc2C2cLEwNkXYMtaT7l7bGrHVVa02k'});
			google.charts.setOnLoadCallback(drawRegionsMap);

			function drawRegionsMap() {
			   var data = google.visualization.arrayToDataTable([
				["Location","Study count"  ],
				<?php
					while($row = mysqli_fetch_assoc($query)) {
						$key = $row["cat"];
						$cnt = $row["cnt"];

						echo "[\"$key\", $cnt],";
					}
				?>

				]);

				var options = {
					sizeAxis: { minValue: 0, maxValue: 2000 },
					region: 'world',
					displayMode: 'markers',
					colorAxis: {colors: ['lightblue', 'darkblue']},
					
					resolution:  'countries'
				};

				var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

				chart.draw(data, options);
			}
		</script>
		<style>
			path {
				stroke: black;
				stroke-width: 1px;
			}
		</style>
	</head>
	<body>
		<?php
			$sql = "SELECT * FROM mesh_term WHERE mesh_id=$term";
			$query = mysqli_query($server, $sql);
			$mesh = mysqli_fetch_assoc($query);
			echo "<h2 style='text-align:center'>Studies with MeSH term ".$mesh['mesh_term']." (".$mesh['tree'].")</h2>";
		?>
		<div id="regions_div" style="width: 1300px; height: 800px;"></div>
	</body>
</html>