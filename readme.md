# h-vis Hierarchical Data Visualization Tool

This project was created as part of a scientific research. The goal is to provide network visualization of clinical trials data.
The related publication is under review, reference will be added after acceptance.

Functions:
* Visualization of research locations by MeSH terms (tree or graph format)
* Visualization of condition hierarchy
* Visualization of intervention hierarchy
* Queries and maps by region and country
* Educational material with map and chart visualizations (Hungarian)

[Running demo](http://ujrr.sote.hu/h-vis)

## Source data
Data was downloaded from [clinicaltrials.gov](http://clinicaltrials.gov). Data cleaning was applied to country and city names of the trial locations. The project contains preprocessed data. (See `/data/diffusion` folder.)

## Installing
* Copy the files to the web/www folder of an Apache or similar web server. Location visualization does not require any additional setup.
* Set up database for hierarchy visualization:
 * Import the contents of `/data/h-vis.sql.zip` to a MySQL database
 * Adjust the connection data in `/common/db.php`

## Built With
* [dhtmlxGrid](https://dhtmlx.com/docs/products/dhtmlxGrid/)
* [d3.js](https://d3js.org/)
* [amCharts](http://www.amcharts.com)

## Screenshots

### Location visualization as graph:
![graph](/screenshots/graph.png)

### Location visualization as tree:
![tree](/screenshots/tree.png)

### Hierarchy visualization:
![hierarchy](/screenshots/hierarchy.png)

### Map visualization (research connections of a selected city):
![hierarchy](/screenshots/hungary_map.PNG)