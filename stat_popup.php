<?php
	include "common/db.php";
	
	$type = $_GET['type'];
	$values = $_GET['values'];
	$mesh_type = $_GET['mesh_type'];
	
	$names = [];
	
	if($type == 'country') {
		$cond = " AND study_id IN (SELECT study_id from location_country WHERE country_id IN ($values))";
		
		$csql = "SELECT * FROM country WHERE country_id IN ($values) ORDER BY name";		
		$query = mysqli_query($server, $csql);
		if ( !$query ) {
			echo mysqli_error($server);
			die;
		}	
		
		while($row = mysqli_fetch_assoc($query)) {
			$names[] = $row['name'];
		}
	} else if($type == 'region') {
		$cond = " AND study_id IN (SELECT study_id from location_country WHERE country_id IN (
			SELECT country_id FROM country WHERE region_id IN ($values)))";
			
		$rsql = "SELECT * FROM region WHERE region_id IN ($values) ORDER BY region_name";		
		$query = mysqli_query($server, $rsql);
		if ( !$query ) {
			echo mysqli_error($server);
			die;
		}	
		
		while($row = mysqli_fetch_assoc($query)) {
			$names[] = $row['region_name'];
		}
	}
	
	$sql = "SELECT count(study_id) AS no_studies, mesh_term FROM study_mesh
				INNER JOIN mesh_term ON study_mesh.mesh_id=mesh_term.mesh_id
				WHERE mesh_term.type=$mesh_type
				$cond 
				GROUP BY study_mesh.mesh_id
				ORDER BY no_studies DESC";
				
//	echo $sql;
	
	$query = mysqli_query($server, $sql);
	if ( !$query ) {
		echo mysqli_error($server);
		die;
	}
?>
<html>
	<head>
		<link rel="stylesheet" href="dhtmlxSuite_v50/codebase/dhtmlx.css" />
		<link rel="stylesheet" href="dhtmlxSuite_v50/skins/web/dhtmlxgrid.css" />
		<link rel="stylesheet" href="css/style.css" />
		<script src="dhtmlxSuite_v50/codebase/dhtmlx.js" ></script>
	</head>
	<body onload="init()">
		<h1>Study count by <?php echo $mesh_type==1 ? "condition" : "intervention";?></h1>
		<h2>Selected <?php echo $type;?>(s): <?php echo implode($names, ","); ?></h2>
	<div id="chart_container" style="width:700px;height:350px;margin:20px auto;border:1px solid #c0c0c0;text-align:center"></div>
	<div id="stat_table_wrapper">
	<table id="table" style="width: 800px;">
		<thead>
			<tr>
				<th sort="int">No.</th>
				<th>Mesh term</th>
				<th sort="int">Study count</th>
			</tr>
		</thead>
		<tbody>
<?php
	$n = 0;
	$max = 0;
	$chartData = array();
	while($row = mysqli_fetch_assoc($query)) {
		$n++;
		$mesh = $row["mesh_term"];
		$cnt = $row["no_studies"];
		echo "<tr>
				<td>$n</td>
				<td>$mesh</td>
				<td>$cnt</td>
			</tr>";
			
		if($n == 1) {
			$max = $cnt;
		}
		if($n <= 20) {
			$chartData[] = [$mesh, $cnt];
		}
	}
?>
		</tbody>
	</table>
	</div>
		<script>
			var chartData = [
				<?php
					foreach($chartData as $c) {
						echo "['$c[0]',$c[1]],";
					}
				?>
			];
		
			var myChart, myGrid;
			function init() {
				myGrid = dhtmlXGridFromTable("table");
				myGrid.enableAutoHeight(true,400);
				myGrid.setSizes();
				
				myChart =  new dhtmlXChart({
					view:"bar",//sets chart's type. In our case it's vertical bar
					color:"#66ccff", //sets color of bars
					container:"chart_container",// an html container that will contain our chart
					// sets data that chart will present. #data0# refers to the first column of the grid
					value:"#data1#", 
					label:"#data1#", 
					radius: 0,
					border: true,
					tooltip: "#data0#",
					//label:"#data0#", // specifies chart labels
					yAxis:{  //sets vertical axis
						start:0,
						step: <?php echo $max>200 ? 50 : 10; ?>,
						end:<?php echo round($max, -1, PHP_ROUND_HALF_UP);?>,
						template:function(obj){
							return obj;
						}						
					},
					/*xAxis:{ 
						template:"#data0#"
					}*/
				});
				myChart.parse(chartData,"jsarray");
				
			}
		</script>
	</body>
</html>