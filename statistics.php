<?php
	$type = $_GET['type'];
?>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="dhtmlxSuite_v50/codebase/dhtmlx.css" />
		<script src="dhtmlxSuite_v50/codebase/dhtmlx.js" ></script>
		
		<!--<script src="https://d3js.org/d3.v4.min.js"></script>
		<script src="js/drawing.js"></script>
		<script src="js/controls.js"></script>-->
		<script>
			var myList;
			function doOnLoad() {
				myList = new dhtmlXList({
					container:"data_container",
					template:"#name#",
					type:{ height: 30, width: 200 }
				});
				myList.load("data/location_filter.php?type=<?php echo $type;?>", "json");
			}
			
			function showStat(stat_type) {
				var filter = myList.getSelected();
				if(filter == "") {
					alert("Choose at least one <?php echo $type; ?>!");
					return;
				}
				var values = filter instanceof Array ? filter.join() : filter;
				var url;
				if(stat_type == 3) {
					if(filter instanceof Array) {
						alert("Please select only one <?php echo $type; ?>! Map mode currently does not work for multiple selection!");
						return;
					}
					url = "map.php?type=<?php echo $type; ?>&values="+values;
				} else {
					url = "stat_popup.php?type=<?php echo $type; ?>&values="+values+"&mesh_type="+stat_type;
				}
				window.open(url);
			}

		</script>
	</head>
	<body onload="doOnLoad();">
		<div id="controls" class="clearfix">
			<p>
				<span class="control_box" id="main_box"><button id="mainButton" onclick="location.href='index.html'">Main menu</button></span>
			</p>
			<div id="filter" style="float:left">
				<p>Select <?php echo $type; ?>:</p>
				<div id="data_container" class="list_demo_samples"></div><br>
				<p>Filter by name: <br><input style="width:230px" type="text" id="filter_txt" onkeyup="myList.filter('#name#', document.getElementById('filter_txt').value)"/></p>
			</div>
			<div id="stat_buttons">
				<p><button onclick="showStat(2)">Intervention list</button></p>
				<p><button onclick="showStat(1)">Condition list</button></p>
				<p><button onclick="showStat(3)">Map</button></p>
			</div>
		</div>
	</body>
</html>