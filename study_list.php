<html>
	<head>
		<link rel="stylesheet" href="dhtmlxSuite_v50/codebase/dhtmlx.css" />
		<link rel="stylesheet" href="dhtmlxSuite_v50/skins/web/dhtmlxgrid.css" />
		<link rel="stylesheet" href="css/style.css" />
		<script src="dhtmlxSuite_v50/codebase/dhtmlx.js" ></script>
	</head>
	<body onload="init()">
		<?php
		$term = $_GET['mesh_id'];

		include "common/db.php";
		
		mysqli_query($server, "SET NAMES UTF8");
		
		$sql = "SELECT * FROM mesh_term WHERE mesh_id=$term";
		$query = mysqli_query($server, $sql);
		$mesh = mysqli_fetch_assoc($query);
		echo "<h2 style='text-align:center'>Studies with MeSH term ".$mesh['mesh_term']." (".$mesh['tree'].")</h2>";
		
		
		$sql = "SELECT * FROM clinical_study
			WHERE study_id IN (
				SELECT study_id FROM study_mesh
					WHERE mesh_id='$term')";
		
		$query = mysqli_query($server, $sql);
		if ( !$query ) {
			echo mysqli_error($server);
			die;
		}
		?>
		<table id="table" multiline="true">
			<thead>
				<tr>
					<th width="80" sort="int">Study id</th>
					<th>Title</th>
					<th width="80" sort="date">Start date</th>
					<th width="80" sort="date">End date</th>
					<th width="200" >Locations</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while($row = mysqli_fetch_assoc($query)) {
					$id = $row["study_id"];
					$nid = $row["nct_id"];
					$title = $row["brief_title"];
					$start = $row["start_date"];
					$end = $row["completion_date"];
					if(!$end) {
						$end = $row["primary_completion_date"];
					}
					
					echo "<tr>
							<td>$id</td>
							<td><a href='https://clinicaltrials.gov/ct2/show/$nid' target='_blank'>$title</a></td>
							<td>$start</td>
							<td>$end</td>";
							
					$lsql = "SELECT * FROM location_cache WHERE study_id=$id";
					$query2 = mysqli_query($server, $lsql);
					
					echo "<td>";
					while($row2 = mysqli_fetch_assoc($query2)) {
						$city = $row2["city_clean"];
						$country = $row2["country_clean"];
						
						echo "$city ($country)<br/>";
					}
					echo "</td>";
					echo "</tr>";
				}
			?>
			</tbody>
		</table>
		
		<script>
			var myGrid;
			function init() {
				myGrid = dhtmlXGridFromTable("table");
				myGrid.enableAutoHeight(true,500);
				myGrid.setSizes();
			}
		</script>
	</body>
</html>